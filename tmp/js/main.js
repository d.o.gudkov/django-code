var loadingState = false

function init(){

    hljs.initHighlightingOnLoad()

    const players = Array.from($('.js-player')).map(p => new Plyr(p))

    $('.lesson-content code').each(function(i, block) {
      hljs.highlightBlock(block)
    })


    $('.fa-thumbs-up').on('click', function(e){
        var t = $(this)
        comment_id = $(this).data('comment-id')
        $.ajax({
            url: '/add-likes/',
            data: {'comment_id': comment_id},
            method: 'GET',
            success: function(data){
                $("#comment-"+comment_id).text(data.likes)
                t.addClass('liked')
            }
        })
    })

    $('.comment-form').on("submit", function(e){
        e.preventDefault()
        $(".menu label").removeClass("fa-bars").addClass("fa-circle-notch fa-spin")
        var t = $(this)
        $.ajax({
            url: t.data('action'),
            data: t.serialize(),
            method: 'POST',
            success: function(data){
                t[0].reset()
                $(".comments-list").prepend(data)
                $('.comments-list-item code').each(function(i, block) {
                  hljs.highlightBlock(block)
                })
                $(".menu label").removeClass("fa-circle-notch fa-spin").addClass("fa-bars")
            }
        })
    })

    $('.collaborate-form').on("submit", function(e){
        e.preventDefault()
        $(".menu label").removeClass("fa-bars").addClass("fa-circle-notch fa-spin")
        var t = $(this)
        $("button:submit").attr("disabled", 'disabled');
        $.ajax({
            url: t.data('action'),
            data: t.serialize(),
            method: 'POST',
            success: function(data){
                if (data.is_send == 1){
                    t.replaceWith("<div class='error'><h5>Собщение успешно отправлено</h5></div>")
                } else {
                    t.prepend("<div class='error'><h5>Произошел сбой при отправке сообщения, попробуйте отправить позже. Или обратитесь напрямую по почте <a href='mailto:hello@djangocode.com'>hello@djangocode.com</a></h5></div>")
                    $("button:submit").removeAttr("disabled");
                }
                $(".menu label").removeClass("fa-circle-notch fa-spin").addClass("fa-bars")
            }
        })
    })

    $('.donate-form').on("submit", function(e){
        e.preventDefault()
        $(".menu label").removeClass("fa-bars").addClass("fa-circle-notch fa-spin")
        var t = $(this)
        $("button:submit").attr("disabled", 'disabled');
        $.ajax({
            url: t.data('action'),
            data: t.serialize(),
            method: 'POST',
            success: function(data){
                console.log(data);
                t.replaceWith("<div class='error'><h5>Спасибо за поддержку</h5></div>")
                $(".menu label").removeClass("fa-circle-notch fa-spin").addClass("fa-bars")
                setTimeout(function(){
                    window.location = t.attr('action') + '?' + t.serialize()
                }, 1000)
                $(".menu label").removeClass("fa-circle-notch fa-spin").addClass("fa-bars")
            }
        })
    })

    var stateload = false
    $('.search-form').on("submit keyup", function(e){
        e.preventDefault()
        if ( stateload ) {
            return null
        }
        if ( $('.search-form input[type="text"]').val().length > 3 ){
            $(".menu label").removeClass("fa-bars").addClass("fa-circle-notch fa-spin")

            var t = $(this)
            stateload = true
            setTimeout(
                function(){
                    $.ajax({
                        url: t.attr('action'),
                        data: t.serialize(),
                        method: 'GET',
                        success: function(data){
                            $(".grid").html(data)
                            load()
                            stateload = false
                        }
                    })
                    $(".menu label").removeClass("fa-circle-notch fa-spin").addClass("fa-bars")
                }, 2000)
        }
    })

    $("a:not([href='#'],[href^='http'],[href^='mailto'],[href^='tel'])").on("click", function(e) {
        e.preventDefault()
        $(".menu label").removeClass("fa-bars").addClass("fa-circle-notch fa-spin")
        var t = $(this)
        var href = t.attr('href')
        $(".main").addClass("load")
        if (loadingState) return false
        loadingState = true
        $.get(href).done(function(data) {
            if ($(window).scrollTop() > 0) {
                $('html,body').animate({
                scrollTop: 0
                }, 300)
            }

            var content = $(data).find('section.main')
            var seo_title = $(data).filter("title").text()
            var seo_keywords = $(data).filter("meta[name='keywords']").attr("content")
            var seo_description = $(data).filter("meta[name='description']").attr("content")
            $('html').find("title").text(seo_title)
            $('html').find("meta[name='keywords']").attr("content", seo_keywords)
            $('html').find("meta[name='description']").attr("content", seo_description)
            $('body').find('main').find('section.main').replaceWith(content)
            history.pushState(null, null, href)
            loadingState = false

            init()
            load()

            // $("#menu-bars").val("off")
            // $("menu").removeClass("open")

            $(".menu label").removeClass("fa-circle-notch fa-spin").addClass("fa-bars")
        }).fail(function() {
            window.location = href
        })
    })


}

function load(){

    $('.placeholder').each(function(){
        var placeholder = $(this),
        small = $(this).find('.img-small')

        // 1: load small image and show it
        var img = new Image()
        img.src = small.attr('src')
        img.onload = function () {
            small.addClass('loaded')
        }

        // 2: load large image
        var imgLarge = new Image()
        imgLarge.src = placeholder.data('large')
        imgLarge.onload = function () {
            $(imgLarge).addClass('loaded')
        }
        placeholder.append(imgLarge)

    })

}

$(function(){

    $(".menu label").on("click", function(){
        $("menu").toggleClass("open")
    })

    init()
})

$(window).on('load', function(){
    load()
})
