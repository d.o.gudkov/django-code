# Generated by Django 2.1.2 on 2018-11-19 20:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0011_auto_20181119_1621'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lesson',
            name='release_link',
        ),
    ]
