from django import forms
from django.contrib.auth.models import User

class RegistrationForm(forms.ModelForm):
    email = forms.CharField(widget=forms.EmailInput(attrs={'autocomplete': 'off', 'autocorrect': 'off', 'autocapitalize': 'none'}))
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    password = forms.CharField(widget=forms.PasswordInput(attrs={'autocomplete': 'off'}))

    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name', 'password', ]

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        self.fields['email'].label = 'E-mail'
        self.fields['first_name'].label = 'Имя'
        self.fields['last_name'].label = 'Фамилия'
        self.fields['password'].label = 'Пароль'

    def clean(self):
        email = self.cleaned_data['email']
        if User.objects.filter(username=email).exists():
            raise forms.ValidationError('Кто то уже зарегистрирован с данной почтой, может это вы?')

class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = 'E-mail'
        self.fields['password'].label = 'Пароль'

    def clean(self):
        username = self.cleaned_data['username']
        password = self.cleaned_data['password']

        if not User.objects.filter(username=username).exists():
            raise forms.ValidationError('Неверный e-mail или пароль')
        user = User.objects.get(username=username)
        if not user.check_password(password):
            raise forms.ValidationError('Неверный e-mail или пароль')

class CommentForm(forms.Form):
    # lesson = forms.CharField(widget = forms.HiddenInput())
    text = forms.CharField(widget=forms.Textarea(attrs={'placeholder': 'Напишите свой бесценный комментарий'}), required=True)

    def __init__(self, *args, **kwargs):
        super(CommentForm, self).__init__(*args, **kwargs)

class SearchForm(forms.Form):
    search = forms.CharField()

    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)

class CollaborateForm(forms.Form):
    name = forms.CharField()
    email = forms.CharField(widget=forms.EmailInput())
    reason = forms.CharField(widget=forms.TextInput())

    def __init__(self, *args, **kwargs):
        super(CollaborateForm, self).__init__(*args, **kwargs)

class DonateForm(forms.Form):
    sum = forms.CharField()
    comment = forms.CharField(widget=forms.TextInput(), required=False)

    def __init__(self, *args, **kwargs):
        super(DonateForm, self).__init__(*args, **kwargs)
