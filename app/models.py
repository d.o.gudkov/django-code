from django.contrib.postgres.fields import ArrayField
from django.conf import settings
from django.db import models
from app.forms import *

def course_upload_folder(instance, filename):
    return "course/{slug}/{file}".format(slug=instance.slug, file=filename)

def lesson_upload_folder(instance, filename):
    return "course/{course_slug}/lessons/{slug}/{file}".format(course_slug=instance.course.slug, slug=instance.slug, file=filename)

def lesson_upd_upload_folder(instance, filename):
    lesson = Lesson.objects.get(updates=instance.id)
    return "course/{course_slug}/lessons/{slug}/updates/{file}".format(course_slug=lesson.course.slug, slug=lesson.slug, file=filename)

# Create your models here.
class Course(models.Model):
    title = models.CharField(verbose_name='заголовок', max_length=255, default='', )
    slug = models.SlugField(verbose_name='ключевое слово',  blank=True, )
    date_add = models.DateTimeField(auto_now_add=True, )
    cover = models.FileField(verbose_name='обложка (большая)', upload_to=course_upload_folder, blank=True, )
    preview = models.FileField(verbose_name='превью', upload_to=course_upload_folder, blank=True, )
    blur = models.FileField(verbose_name='превью (минимальное)', upload_to=course_upload_folder, blank=True, )
    description = models.TextField(verbose_name='описание',)

    class Meta:
        verbose_name = 'курс'
        verbose_name_plural = 'курсы'

    def __str__(self):
        return self.title

class Lesson(models.Model):
    course = models.ForeignKey('Course', on_delete=models.CASCADE, verbose_name='курс',)
    date_add = models.DateTimeField(auto_now_add=True,)
    date_edit = models.DateTimeField(auto_now=True,)
    title = models.CharField(verbose_name='название', max_length=255, default='',)
    slug = models.SlugField(verbose_name='ключевое слово', blank=True,)
    number = models.PositiveIntegerField(verbose_name='порядковый номер', default=1,)
    video = models.FileField(verbose_name='видеоурок', upload_to=lesson_upload_folder, blank=True,)
    captions = models.TextField(verbose_name='субтитры',blank=True,)
    release = models.FileField(verbose_name='релиз', upload_to=lesson_upload_folder, blank=True,)
    release_name = models.CharField(verbose_name='код релиза', max_length=255, default='',)
    release_link = models.URLField(verbose_name='ссылка на релиз', max_length=255, default='',)
    about = models.TextField(verbose_name='описание урока', blank=True,)
    tags = ArrayField(models.CharField(max_length=200), verbose_name='тэги', size=10, blank=True, default=list,)
    updates = models.ManyToManyField('LessonUpdates', verbose_name='обновления', blank=True,)
    notes = models.ManyToManyField('LessonNotes', verbose_name='заметки', blank=True,)

    class Meta:
        verbose_name = 'урок'
        verbose_name_plural = 'уроки'

    def __str__(self):
        return self.title

class LessonUpdates(models.Model):
    file = models.FileField(verbose_name='файл', upload_to=lesson_upd_upload_folder,)

    class Meta:
        verbose_name = 'обновление'
        verbose_name_plural = 'обновления'

    def __str__(self):
        return 'Обновление #{id}'.format(id=self.id)

class LessonNotes(models.Model):
    language = models.CharField(verbose_name='язык', max_length=255,)
    code = models.TextField(verbose_name='код')

    class Meta:
        verbose_name = 'заметка'
        verbose_name_plural = 'заметки'

    def __str__(self):
        return 'Заметка #{id}'.format(id=self.id)

class Comments(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='author', on_delete=models.CASCADE, verbose_name='пользователь', )
    lesson = models.ForeignKey('Lesson', on_delete=models.CASCADE, verbose_name='урок', default=None,)
    date_add = models.DateTimeField(auto_now_add=True,)
    text = models.TextField(verbose_name='текст',)
    likes = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='like_users', verbose_name='понравилось', blank=True,)

    class Meta:
        verbose_name = 'комментарий'
        verbose_name_plural = 'комментарии'

    def like_count(self):
        return self.likes.count()
        # return 0

    def like_add(self, user):
        if not user in self.likes.all():
            self.likes.add(user)
        return self.like_count()

    def __str__(self):
        return self.text

class Donate(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name='пользователь', blank=True, default=settings.DEFAULT_USER_ID)
    sum = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='сумма пожертвования',)
    date_add = models.DateTimeField(auto_now_add=True,)
    comment = models.TextField(verbose_name='комментарий', blank=True,)

    class Meta:
        verbose_name = 'пожертвование'
        verbose_name_plural = 'пожертвования'

    def __str__(self):
        return "Пожертвование #{id} на сумму {sum}".format(id=self.id, sum=self.sum)
