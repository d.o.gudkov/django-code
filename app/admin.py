from django.contrib import admin
from app.models import *
from app.widgets import *

class CourseAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ('title', 'slug','id',)

class LessonAdmin(admin.ModelAdmin):
    formfield_overrides = {
        # models.TextField: {'widget': CKEditorWidget},
        # JSONField: {'widget': JSONModelAdminForm},
    }
    prepopulated_fields = {"slug": ("title",)}
    list_display = ('title', 'slug','id',)

class CommentsAdmin(admin.ModelAdmin):
    list_display = ('text', 'date_add', )

# Register your models here.
admin.site.register(Course, CourseAdmin)
admin.site.register(Lesson, LessonAdmin)
admin.site.register(LessonUpdates)
admin.site.register(LessonNotes)
admin.site.register(Donate)
admin.site.register(Comments, CommentsAdmin)
