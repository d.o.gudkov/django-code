from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from django.conf import settings
from django.contrib.auth import login as auth_login, logout as auth_logout, authenticate
from django.contrib.auth.models import User
from django.contrib.postgres.search import SearchVector
from django.core.mail import send_mail
from django.utils.html import strip_tags
from app.forms import *
from app.models import *
import requests


# Create your views here.
def home(request):
    lessons = Lesson.objects.all()
    return render(request, 'base.html', { 'lessons': lessons })

def courses(request):
    courses = Course.objects.all()
    return render(request, 'courses/list.html', {'courses': courses})

def course(request, course):
    course = Course.objects.get(slug=course)
    lessons = Lesson.objects.filter(course=course.id)
    return render(request, 'courses/main.html', {'course': course, 'lessons': lessons })

def lesson(request, course, lesson):
    lesson = Lesson.objects.get(slug=lesson)
    lessons = Lesson.objects.filter(course=lesson.course.id)
    form = CommentForm(request.POST or None)
    if form.is_valid():
        text = form.cleaned_data['text']
        comment = Comments(
            user = request.user,
            lesson = lesson,
            text = text,
        )
        comment.save()
        return render(request, 'ajax/comment.html', {'comment': comment })
    comments = Comments.objects.filter(lesson=lesson).order_by('-date_add')
    return render(request, 'courses/lesson.html', {'lesson': lesson, 'lessons': lessons, 'form': form, 'comments': comments, 'user': request.user })

def login(request):
    login_user = False
    form = LoginForm(request.POST or None)
    if request.user.id:
        return HttpResponseRedirect(reverse('profile'))
    if request.GET.get('code'):
        r = requests.post('https://graph.facebook.com/oauth/access_token', data = { 'client_id': settings.FACEBOOK_KEY, 'client_secret': settings.FACEBOOK_SECRET, 'code': request.GET.get('code'), 'redirect_uri': settings.FACEBOOK_REDIRECT,})
        u = requests.get('https://graph.facebook.com/me', params = { 'access_token': r.json()['access_token'], 'fields': 'id,first_name,last_name,birthday,email' })
        username, first_name, last_name = (u.json()['email'], u.json()['first_name'], u.json()['last_name'])
        if not User.objects.filter(username=username).exists():
            new_user = User(
                username = username,
                first_name = first_name,
                last_name = last_name,
            )
            new_user.set_password(settings.FACEBOOK_USER_SECRET)
            new_user.save()
        login_user = authenticate(username=username, password=settings.FACEBOOK_USER_SECRET)
    if form.is_valid():
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        login_user = authenticate(username=username, password=password)
    if login_user:
        auth_login(request, login_user)
        return HttpResponseRedirect(reverse('profile'))
    form = LoginForm(request.POST or None)
    params = {
        'client_id': settings.FACEBOOK_KEY,
        'redirect_uri': settings.FACEBOOK_REDIRECT,
        'response_type': 'code',
        'scope': 'email',
        'state': 'fb'
    }
    return render(request, 'profile/login.html', {'form': form, 'fb_url' : requests.get(settings.FACEBOOK_OAUTH, params=params).url })

def logout(request):
    auth_logout(request)
    return HttpResponseRedirect(reverse('home'))

def registration(request):
    if request.user.id:
        return HttpResponseRedirect(reverse('profile'))
    form = RegistrationForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data['email']
        password = form.cleaned_data['password']
        new_user = User(
            username = username,
            first_name = form.cleaned_data['first_name'],
            last_name = form.cleaned_data['last_name'],
        )
        new_user.set_password(password)
        new_user.save()
        login_user = authenticate(username=username, password=password)
        if login_user:
            auth_login(request, login_user)
            return HttpResponseRedirect(reverse('profile'))
    return render(request, 'profile/registration.html', {'form': form})

def profile(request):
    if not request.user.id:
        return HttpResponseRedirect(reverse('login'))
    user = User.objects.get(id=request.user.id)
    courses = Course.objects.all()[:3]
    comments = Comments.objects.filter(user=request.user)
    likes = 0
    for comment in comments:
        likes += comment.like_count()
    return render(request, 'profile/profile.html', {'user': user, 'courses': courses, 'comments': len(comments), 'likes': likes})

def profile_edit(request):
    if not request.user.id:
        return HttpResponseRedirect(reverse('login'))
    user = User.objects.get(id=request.user.id)
    courses = Course.objects.all()[:3]
    comments = Comments.objects.filter(user=request.user)
    likes = 0
    for comment in comments:
        likes += comment.like_count()
    return render(request, 'profile/edit.html', {'user': user, 'courses': courses, 'comments': len(comments), 'likes': likes})


def captions(request, id):
    content = Lesson.objects.get(id=id).captions
    response = HttpResponse("", content_type="text/vtt; charset=utf-8")
    response.write(content)
    return response

def like(request):
    comment_id = request.GET.get('comment_id')
    comment = Comments.objects.get(id=comment_id)
    likes = comment.like_add(user=request.user)
    return JsonResponse({ 'likes': likes })

def search(request):
    form = SearchForm(request.GET or None)
    if form.is_valid():
        search = form.cleaned_data['search']
        lessons = Lesson.objects.annotate(
            search=SearchVector('title', 'captions', 'about', 'course__title', 'course__description'),
        ).filter(search=search)
        return render(request, 'ajax/search.html', {'lessons': lessons})
    return render(request, 'search.html', {'form': form})

# Simple text pages
def donate(request):
    donates = len(Donate.objects.all()) + 1
    form = DonateForm(request.POST or None)
    if form.is_valid():
        sum = form.cleaned_data['sum']
        comment = form.cleaned_data['comment']
        donate = Donate(
            sum = sum,
            comment = comment,
        )
        if request.user.id:
            donate.user = User.objects.get(id=request.user.id)
        donate.save()
        return JsonResponse({'ok': 1})
    return render(request, 'donate.html', {'form': form, 'donates': donates})

def collaborate(request):
    form = CollaborateForm(request.POST or None)
    if form.is_valid():
        name = form.cleaned_data['name']
        email = form.cleaned_data['email']
        reason = form.cleaned_data['reason']
        message = '<b>Имя</b>: {name}<br /><b>Email</b>: {email}<br /><b>Причина</b>: {reason}'.format(name=name, email=email, reason=reason,)
        is_send = send_mail(
            'Заявка с формы коллобарации',
            strip_tags(message),
            'hello@djangocode.com',
            ['d.o.gudkov@gmail.com'],
            fail_silently=True,
            html_message=message,
        )
        return JsonResponse({'is_send': is_send, })
    return render(request, 'collaborate.html', {'form': form})

def about(request):
    return render(request, 'about.html', {})

def privacy(request):
    return render(request, 'privacy-policy.html', {})

def terms(request):
    return render(request, 'terms-of-use.html', {})

# Error pages
def page_not_found_view(request, exception=None):
    return render(request, 'errors/404.html', {})

def error_view(request):
    return render(request, 'errors/500.html', {})
